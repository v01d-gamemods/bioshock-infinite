from trainerbase.codeinjection import CodeInjection, AllocatingCodeInjection
from trainerbase.memory import pm


infinite_ammo = AllocatingCodeInjection(
    pm.base_address + 0x5F9EAC,
    """
        mov eax, 9999
        mov [esi + 0x7C4], eax
    """,
    original_code_length=6,
)

get_money_on_spend = AllocatingCodeInjection(
    pm.base_address + 0x4BED25,
    """
        mov dword [esi + 0x54], 99999
        push 0x10
        mov ecx, esi
    """,
    original_code_length=7,
)
