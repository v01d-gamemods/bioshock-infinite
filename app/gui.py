import dearpygui.dearpygui as dpg

from trainerbase.gui import add_codeinjection_to_gui, simple_trainerbase_menu
from injections import *


@simple_trainerbase_menu("BioShock Infinite", 300, 300)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Code Injections", tag="code_injections"):
            add_codeinjection_to_gui(infinite_ammo, "Infinite ammo")
            add_codeinjection_to_gui(get_money_on_spend, "Get money on spend")
